var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var messageSchema = mongoose.Schema({
    title:      {type : String},
    price:      {type : Number},
    auteur:     {type : String},
    annonceText:{type : String},
    date:       {type : Date, default: Date.now },
    longitude:  {type : Number},
    latitude:   {type : Number},
    });

mongoose.model('Advert', messageSchema);