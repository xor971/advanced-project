var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
	active:		{type : Number},
	username:	{type : String},
	firstName:	{type : String},
	name:		{type : String},
    email:      {type : String},
    dateSignup:	{type : Date},
    lastLogged:	{type : Date},
    password:   {type : String}
    });
	
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};


userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

mongoose.model('User', userSchema);