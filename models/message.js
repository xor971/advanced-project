var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var messageSchema = mongoose.Schema({
    ToUser:      {type : String},
    Read: {type : Number},
    FromUser:{type : String},
    InResponseToMessageId:{type : String},
    Content:  {type : String},
    Title: { type : String },
    });

mongoose.model('Message', messageSchema);