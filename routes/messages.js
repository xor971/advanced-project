var mongoose = require('mongoose');
var Message = mongoose.model('Message');


// Créer des messages
exports.createMessage = function (req, res) {
    var itsOk = true;
    
    var newMessage = new Message();
    newMessage.ToUser = req.body.ToUser;
    newMessage.Title = req.body.Title;
    newMessage.Read = 0;
    newMessage.FromUser = req.user.username;
    newMessage.InResponseToMessageId = req.body.InResponseToMessageId;
    newMessage.Content = req.body.Content;
    
    newMessage.save(function (err) {
        if (err) {
            itsOk = false;
            return res.json(400, err);
        }
    });
    res.send(itsOk);
};


//Récupérer des messages
exports.getMessages = function (req, res) {
    Message.find({ ToUser: req.user.username, InResponseToMessageId: 0 }, function (err, resulltt) {
        if (err) {
            res.send(res.json(400, err));
        } else {
            res.send(result);
        }
    });
};


//Récupérer les messages envoyer
exports.getSentMessages = function (req, res) {
    Message.find({ FromUser: req.user.username, InResponseToMessageId: 0 }, function (err, result) {
        if (err) {
            res.send(res.json(400, err));
        } else {
            res.send(result);
        }
    });
};


//Récupérer les messages non lu
exports.getUnreadMessages = function (req, res) {
    Message.find({ ToUser: req.user.username, Read : 0 , InResponseToMessageId: 0 }, function (err, result) {
        if (err) {
            res.send(res.json(400, err));
        } else {
            res.send(result);
        }
    });
};

//Récupérer les messages envoyer
exports.getMessageReplies = function (req, res) {
    Message.find({ InResponseToMessageId: req.body.InResponseToMessageId }, function (err, result) {
        if (err) {
            res.send(res.json(400, err));
        } else {
            res.send(result);
        }
    });
};


//Marquer les messages lu comme lu
exports.markMessageRead = function (req, res) {
    Message.update({ _id: req.body.messageId }, { $set: { Read: 1 } }, function (err, resullt) {
        if (err) {
            res.send(res.json(400, err));
        } else {
            res.send(resullt);
        }
    });
};


//Répondre à un message qui m'est destiné
exports.replyToMessage = function (req, res) {
    
    var itsOk = true;
    
    var newMessage = new Message();
    newMessage.ToUser = req.body.ToUser;
    newMessage.Read = 0;
    newMessage.FromUser = req.user.username;
    newMessage.InResponseToMessageId = req.body.InResponseToMessageId;
    newMessage.Content = req.body.Content;
    
    newMessage.save(function (err) {
        if (err) {
            itsOk = false;
            return res.json(400, err);
        }
    });
    res.send(itsOk);
};