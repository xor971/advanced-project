var mongoose	= require('mongoose');
var User 		= mongoose.model('User');

exports.index = function(req, res) {
	res.sendfile('Views/Layout.html');
};

exports.profil = function(req, res) {
	res.json(req.user);
};


exports.otherProfilInfo = function(req, res) {
	var name = req.params.name;

	User.findOne({ 'username' :  name}, function(err, user) {
		if (err)
			return res.json(400, err);

		if (user) {
			res.json(user);
		} else {
			res.json(400, 'USER_NOT_FOUND');
		}
	});
};