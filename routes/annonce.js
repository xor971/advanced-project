var mongoose = require('mongoose');
var Advert = mongoose.model('Advert');


//Creer une annonce
exports.buildAnnonce = function (req, res) {
    var date = new Date();
    var itsOk = true;
    
    var newAdvert = new Advert();
    newAdvert.annonceText = req.body.annonceText;
    newAdvert.title = req.body.title;
    newAdvert.date = date;
    newAdvert.price = req.body.price;
    newAdvert.longitude = req.body.longitude;
    newAdvert.latitude = req.body.latitude;
    newAdvert.auteur = req.user.username;
    
    newAdvert.save(function (err) {
        if (err) {
            itsOk = false;
            return res.json(400, err);
        }
    });
    res.send(itsOk);
};


//Afficher toute les annonces
exports.allAnnonce = function (req, res) {
    Advert.find(function (err, result) {
        if (err) {
            res.send(res.json(400, err));
        }
        else {
            res.send(result);
        }
    });

}


//Afficher seulement mes annonces
exports.myAnnonce = function (req, res) {
    Advert.find({ auteur : req.user.username }, function (err, result) {
        if (err) {
            res.send(res.json(400, err));
        } else {
            res.send(result);
        }
    });
    

}