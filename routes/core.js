module.exports = function (my_express, passport) {
    
    //Gestions de toute les routes
    var routes = require('./index');
    my_express.get('/', routes.index);
    my_express.get('/profil', auth, routes.profil);
    my_express.get('/profil/:name', routes.otherProfilInfo);
    
    var testLoggedin = require('./loggedin');
    my_express.get('/loggedin', testLoggedin.loggedin);
    
    var login = require('./login');
    my_express.post('/login', passport.authenticate('my_login'), login.log);
    
    var signup = require('./signup');
    my_express.post('/signup', signup.log);
    
    var logout = require('./logout');
    my_express.post('/logout', logout.disconnect);
    
    var annonce = require('./annonce');
    my_express.post('/annonce', annonce.buildAnnonce);
    my_express.post('/allAnnonce', annonce.allAnnonce);
    my_express.get('/myAnnonce', annonce.myAnnonce);
    
    
    var messages = require('./messages');
    my_express.post('/message', messages.createMessage);
    my_express.post('/replyToMessage', messages.replyToMessage);
    my_express.post('/markeMessageRead', messages.markMessageRead);
    my_express.get('/sentMessages', messages.getSentMessages);
    my_express.get('/messages', messages.getMessages);
    my_express.get('/unreadMessages', messages.getUnreadMessages);
    my_express.post('/messageReplies', messages.getMessageReplies);
    
my_express.get('/auth/facebook',
  passport.authenticate('facebook', { scope: [ 'email' ] }),
  function(req, res){
  });
my_express.get('/auth/facebook/callback', 
  passport.authenticate('facebook', { failureRedirect: '/#!' }),
  function(req, res) {
    res.redirect('/#!/members');
  });

    var error = require('./404');
    my_express.get('*', error._404);
    
    //Middleware
    function auth(req, res, next) {
        if (!req.isAuthenticated())
            res.send(401);
        else
            next();
    }
};
