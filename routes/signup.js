var mongoose	= require('mongoose');
var User 		= mongoose.model('User');
var crypto		= require('crypto');

//Inscription de l'utulisateur

exports.log = function(req, res) {
	process.nextTick(function() {
		User.findOne({ 'email' :  req.body.email}, function(err, user) {
			if (err)
				return res.json(400, err);

			if (user)
				return res.json({exists: true});

			User.findOne({ 'usename' :  req.body.username}, function(err, user) {
				if (err)
					return res.json(400, err);

				if (user)
					return res.json({exists: true});

				var newUser       = new User();
				newUser.email     = req.body.email.toLowerCase();
				newUser.password  = newUser.generateHash(req.body.password);
				newUser.username  = req.body.username.toLowerCase();
				var firstNSplit	  = req.body.firstName.slice(0, 1);
				var firstNSplit2  = req.body.firstName.slice(1);
				newUser.firstName = firstNSplit.toUpperCase() + firstNSplit2.toLowerCase();
				var nameSplit	  = req.body.name.slice(0, 1);
				var nameSplit2	  = req.body.name.slice(1);
				newUser.name 	  = nameSplit.toUpperCase() + nameSplit2.toLowerCase();
				newUser.active	  = 0;
				newUser.dateSignup= new Date();
				newUser.lastLogged=null;

				newUser.save(function(err) {
					if (err){
							return res.json(400, err);
					}
				});
				return res.json({exists: false});	
			});	
		});
	});
}