var mongoose	= require('mongoose');
var User 		= mongoose.model('User');

exports.log = function(req, res) {
	var date = new Date();
	User.update({username : req.user.username}, {lastLogged : date}, function (err, r){
		  if (err)  return res.json(400, err);
		  });
	res.send(req.user);
};

//enregistrement de la dernière connexion de l'utilisateur