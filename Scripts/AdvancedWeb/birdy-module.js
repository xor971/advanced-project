﻿        //The Application module.
var CoinsMarket = angular.module('CoinsMarket', ['ngRoute', 'ngAnimate', 'ngCookies', 'ngResource', 'ui.bootstrap', 'toaster', 'uiGmapgoogle-maps', 'geolocation', 'angularReverseGeocode'])
    .config([
    '$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
                .when('/', { templateUrl: '/Views/pages/home.html', controller: 'homeController' })
                .when('/profile/:name', { templateUrl: '/Views/pages/profile.html', controller: 'profileController', resolve: { loggedin: checkLoggedin } })
                .when('/members', { templateUrl: '/Views/Members/home.html', controller: 'membersHomeController', resolve: { loggedin: checkLoggedin } })
                .when('/messages', { templateUrl: '/Views/Members/Messages.html', controller: 'membersMessagesController', resolve: { loggedin: checkLoggedin } })
                .when('/postAd', { templateUrl: '/Views/Members/AdvertCreate.html', controller: 'membersAdvertCreationController', resolve: { loggedin: checkLoggedin } })
        ;
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('!');
        $httpProvider.responseInterceptors.push(function ($q, $location) {
            return function (promise) {
                return promise.then(
          // Success: just return the response
                    function (response) {
                        return response;
                    }, 
          // Error: check the error status to get only the 401
          function (response) {
                        if (response.status === 401)
                            $location.url('/');
                        return $q.reject(response);
                    }
                );
            }
        });
    }]).run(['$rootScope', '$http', function ($rootScope, $http) {
        $http.get('/loggedin').success(function (user) {
            if (user !== '0') {
                $rootScope.$broadcast("loggedin", user);
            }
            else {
                $rootScope.$broadcast("notLoggedin");
            }
        });
    }]);

//verifies if the user is authorized upon any request.
var checkLoggedin = function ($q, $timeout, $http, $location, $rootScope, toaster) {
    var deferred = $q.defer();
    $http.get('/loggedin').success(function (user) {
        if (user !== '0') {
            $timeout(deferred.resolve, 0);
            $rootScope.$broadcast("loggedin", user);
        }
        else {
            toaster.clear();
            toaster.pop('error', "Error", "You must be logged in to access this page.");
            $timeout(function () { deferred.reject(); }, 0);
            $location.url('/');
            $rootScope.$broadcast("notLoggedin");
        }
    });
    return deferred.promise;
};
