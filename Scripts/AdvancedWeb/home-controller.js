﻿//Controller For the Home page of unregistered users.

CoinsMarket.controller('homeController', ['$scope', '$http', '$location', 'userRepository', 'membersRepository', 'toaster', function ($scope, $http, $location, userRepository, membersRepository, toaster) {
        
        $scope.title = "Blue Bird's List";
        $scope.DisplayMessages = {};
        
        $scope.$on("loggedin", function ($event, user) {
            $scope.authenticatedUser = user;
            $scope.authenticated = true;
        });
        
        $scope.$on("notLoggedin", function ($event) {
            $scope.authenticatedUser = null;
            $scope.authenticated = false;
        });
        
        
        //Catches a page change event and change the webpage title accordingly.
        //Also fetches any new messages.
        $scope.$on("changedPage", function ($event, title) {
            $scope.title = title;
            membersRepository.GetMessages().then(function (response) {
                $scope.DisplayMessages = response;
                $scope.unreadMessages = [];
                angular.forEach($scope.DisplayMessages, function (message) {
                    if (message.Read == false) {
                        $scope.unreadMessages.push(message);
                        console.log("Unread message found");
                    }
                    else {
                        membersRepository.GetMessageReplies(message._id).then(function (replies) {
                            $scope.replies = {};
                            $scope.replies = replies;
                            angular.forEach($scope.replies, function (reply) {
                                if (!reply.Read && reply.FromUser != $scope.authenticatedUser.username) {
                                    console.log("this message contain unread replies");
                                    $scope.unreadMessages.push(message);
                                }
                            });
                        }).catch(function () {
                            console.log("Something went wrong in getting message replies.");
                        });
                    }
                });
                membersRepository.GetSentMessages().then(function (sentMessages) {
                    $scope.sentMessages = [];
                    $scope.sentMessages = sentMessages;
                    angular.forEach($scope.sentMessages, function (sentMessage) {
                        membersRepository.GetMessageReplies(sentMessage._id).then(function (replies) {
                            $scope.replies = {};
                            $scope.replies = replies;
                            angular.forEach($scope.replies, function (reply) {
                                if (!reply.Read && reply.FromUser != $scope.authenticatedUser.username) {
                                    console.log("this message contain unread replies");
                                    $scope.unreadMessages.push(sentMessage);
                                }
                            });
                        }).catch(function () {
                            console.log("Something went wrong in getting sent messages");
                        });
                    });
                });
            });
        });
        
        
        $scope.authenticated = false;
        $scope.authenticatedUser = {};
        $scope.user = {};
        
        //Logs the user out.
        $scope.logout = function () {
            $scope.authenticated = false;
            $scope.authenticatedUser = null;
            $http.post('/logout').then(function () {
                $scope.title = "Blue Bird's List";
            });
        };
        
        //Logs the user in.
        $scope.login = function () {
            toaster.clear();
            toaster.pop('wait', "Wait", "Signing you in", 0);
            userRepository.Login($scope.user).then(function (response) {
                toaster.clear();
                toaster.pop('success', "Success", "Successfully signed in");
                $scope.authenticatedUser = response;
                $scope.authenticated = true;
                $location.url('/members');
            }).catch(function () {
                toaster.clear();
                toaster.pop('error', "Error", "Wrong email password combination");
            });
        };
        
        var verifValuePassword = false;
        var verifValueEmail = false;
        $scope.$watch('[user.password, user.password2]', function (newVal, oldVal) {
            if ($scope.user.password)
                if ($scope.user.password2 == $scope.user.password) {
                    $scope.verif = "passwords match";
                    verifValuePassword = true;
                }
                else {
                    $scope.verif = "passwords unmatch";
                    verifValuePassword = false;
                }
        }, true);
        $scope.$watch('[user.email, user.email2]', function (newVal, oldVal) {
            if ($scope.user.email)
                if ($scope.user.email2 == $scope.user.email) {
                    $scope.verif = "emails match";
                    verifValueEmail = true;
                }
                else {
                    $scope.verif = "emails unmatch";
                    verifValueEmail = false;
                }
        }, true);
        
        //Registers a user.
        $scope.signup = function () {
            if (verifValuePassword && verifValueEmail)
                $http.post('/signup', {
                    email: $scope.user.email,
                    username: $scope.user.username,
                    password: $scope.user.password,
                    firstName: $scope.user.firstName,
                    name: $scope.user.name,
                })
                    .success(function (data) {
                    if (!data.exists) {
                        toaster.clear();
                        toaster.pop('success', "Success", "Successfully signed up");
                        userRepository.Login($scope.user).then(function (response) {
                            $location.url('/members');
                        }).catch(function () {
                            toaster.clear();
                            toaster.pop('error', "Error", "Wrong email password combination");
                        });
                    } else {
                        toaster.clear();
                        toaster.pop('error', "Error", "A user with that email is already registered");
                    }

                })
                    .error(function (data) {
                    $scope.data = data;
                    $scope.message = 'signup failed.';
                    $location.url('/signup');
                });
            else
                console.log("Missing informations");
        };
        
    }]);