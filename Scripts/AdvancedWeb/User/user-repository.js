﻿CoinsMarket.factory('userRepository', ['$http', '$q', function ($http, $q) {
        //Access to rest services for unregistered user.
        return {
            //Register a user.
            Register: function (value) {
                var deferred = $q.defer();
                $http.post('/signup',
            {
                    Login: value.Login,
                    Password: value.Password
                },
            { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            //logs a user in.
            Login: function (value) {
                var deferred = $q.defer();
                $http.post('/login',
                {
                    email: value.email,
                    password: value.password,
                    rememberMe: value.rememberMe
                },
            { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }
        };
    }]);