﻿CoinsMarket.controller('membersMessagesController', ['$scope', 'membersRepository', function ($scope, membersRepository) {
        
        console.log($scope.authenticatedUser.username);
        $scope.messages = [];
        $scope.replies = [];
        $scope.replyToSend = {};
        
        $scope.$emit("changedPage", "Messages");
        
        $scope.selected = false;
        
        membersRepository.GetMessages().then(function (response) {
            $scope.messages = response;
            membersRepository.GetSentMessages().then(function (sentMessages) {
                angular.forEach(sentMessages, function (sentMessage) {
                    $scope.messages.push(sentMessage);
                    angular.forEach($scope.messages, function (message) {
                        membersRepository.GetMessageReplies(message._id).then(function (replies) {
                            $scope.replies = {};
                            $scope.replies = replies;
                            angular.forEach($scope.replies, function (reply) {
                                if (!reply.Read && reply.FromUser != $scope.authenticatedUser.username) {
                                    message.unreadReplies = true;
                                    message.Read = false;
                                }
                            });
                        }).catch(function () {
                            console.log("Something went wrong in getting message replies.");
                        });
                    });
                });
            }).catch(function () {
                console.log("Something went wrong in getting sent messages.");
            });
            angular.forEach($scope.messages, function (message) {
                membersRepository.GetMessageReplies(message._id).then(function (replies) {
                    $scope.replies = {};
                    $scope.replies = replies;
                    angular.forEach($scope.replies, function (reply) {
                        if (!reply.Read && reply.FromUser != $scope.authenticatedUser.username) {
                            message.unreadReplies = true;
                            message.Read = false;
                        }
                    });
                }).catch(function () {
                    console.log("Something went wrong in getting message replies.");
                });
            });
        }).catch(function () {
            console.log("Something went wrong in getting messages.");
        });
        
        $scope.displayMessage = function (message) {
            if (!message.Read && message.FromUser != $scope.authenticatedUser.username)
                membersRepository.MarkMessageRead(message._id);
            if (!message.show) {
                $scope.selected = true;
                angular.forEach($scope.messages, function (messageInList) {
                    if (messageInList != message)
                        messageInList.hide = true;
                    else
                        messageInList.show = true;
                });
                membersRepository.GetMessageReplies(message._id).then(function (response) {
                    $scope.replies = response;
                    $scope.$emit("changedPage", "Messages");
                    angular.forEach($scope.replies , function (reply) {
                        if (!reply.Read && reply.FromUser != $scope.authenticatedUser.username)
                            membersRepository.MarkMessageRead(reply._id);
                    });
                }).catch(function () {
                    console.log("Something went wrong in getting message replies.");
                });
            } else {
                $scope.selected = false;
                angular.forEach($scope.messages, function (messageInList) {
                    messageInList.hide = false;
                    messageInList.show = false;
                });
            }
        };
        
        $scope.showThemAll = function () {
            $scope.selected = false;
            membersRepository.GetMessages().then(function (response) {
                $scope.messages = response;
                membersRepository.GetSentMessages().then(function (sentMessages) {
                    angular.forEach(sentMessages, function (sentMessage) {
                        $scope.messages.push(sentMessage);
                    });
                }).catch(function () {
                    console.log("Something went wrong in getting sent messages.");
                });
            }).catch(function () {
                console.log("Something went wrong in getting messages.");
            });
        };
        
        $scope.reply = function (message) {
            $scope.replyToSend.Id = message._id;
            $scope.replyToSend.ToUserId = message.FromUserId;
            membersRepository.ReplyToMessage($scope.replyToSend).then(function () {
                membersRepository.GetMessageReplies(message._id).then(function (response) {
                    $scope.replies = response;
                    $scope.replyToSend.Content = "";
                }).catch(function () {
                    console.log("Something went wrong in getting message replies.");
                });
            }).catch(function () {
                console.log("Something went wrong replying to message.");
            });
        
        };

    }]);