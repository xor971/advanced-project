﻿CoinsMarket.factory('membersRepository', ['$http', '$q', function ($http, $q) {
        //Access to rest services for registered users
        
        return {
            //return the profile of a registered user
            GetProfile: function (userName) {
                var deferred = $q.defer();
                $http.get('/profil/' + userName,
                {
                    cache: false
                }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            //get unread messages of a user
            GetDisplayMessages: function () {
                var deferred = $q.defer();
                $http.get('/unreadMessages/',
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            //get all messages of a user
            GetMessages: function () {
                var deferred = $q.defer();
                $http.get('/messages/',
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            //get only sent messages of a user
            GetSentMessages: function () {
                var deferred = $q.defer();
                $http.get('/sentMessages/',
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            //get all replies of a user message
            GetMessageReplies: function (messageId) {
                var deferred = $q.defer();
                $http.post('/messageReplies/',
                { InResponseToMessageId: messageId },
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }, 
            //Allows a user to post an advert
            PostAdvert: function (advert) {
                var deferred = $q.defer();
                $http.post('/annonce/',
                {
                    title: advert.Title,
                    annonceText: advert.Content,
                    price: advert.Price,
                    longitude: advert.longitude,
                    latitude: advert.latitude,
                },
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }, 
            //Retrieves all the adverts
            GetAdverts: function () {
                var deferred = $q.defer();
                $http.post('/allAnnonce/',
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }, 
            //Retrieves all the adverts for a given user
            GetUserAdverts: function () {
                var deferred = $q.defer();
                $http.get('/myAnnonce/',
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }, 
            //Send a message
            SendMessage: function (message) {
                var deferred = $q.defer();
                $http.post('/message/',
                {
                    Title: message.Title,
                    ToUser: message.ToUserId,
                    InResponseToMessageId: 0,
                    Content: message.Content,
                },
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }, 
            //reply to a message
            ReplyToMessage: function (message) {
                var deferred = $q.defer();
                $http.post('/replyToMessage/',
                {
                    ToUser: message.ToUserId,
                    InResponseToMessageId: message.Id,
                    Content: message.Content,
                },
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }, 
            //Mark a message read
            MarkMessageRead: function (messageId) {
                var deferred = $q.defer();
                $http.post('/markeMessageRead/',
                {
                    messageId: messageId,
                },
                { cache: false }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }
        };
    }]);