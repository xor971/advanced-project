﻿CoinsMarket.controller('membersHomeController', ['$scope', 'membersRepository', 'toaster', function ($scope, membersRepository, toaster) {
        
        $scope.$emit("changedPage", "Members");
        
        $scope.Adverts = {};
        $scope.message = {};
        $scope.loading = true;
        
        membersRepository.GetAdverts().then(function (response) {
            $scope.Adverts = response;
            $scope.loading = false;
        }).catch(function () {
            $scope.loading = false;
            console.log("Something went wrong.");
        });
        
        
        $scope.displayMap = function (advert) {
            advert.center = { latitude: advert.latitude, longitude: advert.longitude };
            advert.marker = {
                id: advert._id,
                coords: {
                    latitude: advert.latitude,
                    longitude: advert.longitude
                }
            };
            advert.center.zoom = 15;
            advert.showMap = true;
        };
        
        membersRepository.GetUserAdverts();
        
        $scope.hideMap = function (advert) {
            advert.showMap = false;
        };
        
        $scope.createMessage = function (author) {
            console.log(author);
            $scope.message.ToUserId = author;
            $scope.message.Title = "";
            $scope.message.Content = "";
        };
        
        $scope.cancel = function () {

        };
        
        $scope.send = function () {
            toaster.pop('wait', "Wait", "Sending message", 0);
            membersRepository.SendMessage($scope.message).then(function () {
                toaster.clear();
                toaster.pop('success', "Success", "Message sent successfully");
            }).catch(function () {
                toaster.clear();
                toaster.pop('error', "Error", "Something went wrong...");
            });
            $('#messageCreationModal').modal('hide');
        };

    }]);