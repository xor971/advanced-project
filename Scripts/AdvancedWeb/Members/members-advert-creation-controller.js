﻿CoinsMarket.controller('membersAdvertCreationController', ['$scope', 'membersRepository', 'geolocation', '$timeout', 'toaster', function ($scope, membersRepository, geolocation, $timeout, toaster) {
        
        $scope.$emit("changedPage", "Create Advert");
        
        $scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 15 };
        $scope.marker = {};
        $scope.marker.options = {
            draggable: true
        };
        
        $scope.disableLocation = function () {
            $scope.locationEnabled = false;
        };
        
        $scope.lat = {};
        $scope.lon = {};
        $scope.enableLocation = function () {
            console.log("locating");
            if (navigator.geolocation) {
                console.log("geolocating");
                navigator.geolocation.getCurrentPosition(function (position) {
                    $scope.locationEnabled = true;
                    $scope.setUserPosition(position);
                    $scope.$apply();
                });
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        };
        
        $scope.setUserPosition = function (position) {
            $scope.geocoder = new google.maps.Geocoder();
            
            $scope.map.center.latitude = position.coords.latitude;
            $scope.map.center.longitude = position.coords.longitude;
            $scope.lat = position.coords.latitude;
            $scope.lon = position.coords.longitude;
            $scope.marker = {
                id: 0,
                coords: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                },
                options: { draggable: true },
            };
        };
        $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
            if (_.isEqual(newVal, oldVal))
                return;
            else {
                var latlng = new google.maps.LatLng($scope.marker.coords.latitude, $scope.marker.coords.longitude);
                $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $scope.addr = results[0].formatted_address;
                            $scope.$apply();
                        } else {
                            $scope.addr = "No results found: Please replace the marker...";
                            $scope.$apply();
                        }
                    }
                    else {
                        $scope.addr = "No results found: Please replace the marker...";
                        $scope.$apply();
                    }
                });

            }
        });
        
        $scope.post = function () {
            if ($scope.locationEnabled) {
                $scope.advert.latitude = $scope.marker.coords.latitude;
                $scope.advert.longitude = $scope.marker.coords.longitude;
            } else {
                $scope.advert.latitude = 0;
                $scope.advert.longitude = 0;
            }
            console.log($scope.advert);
            toaster.clear();
            toaster.pop('wait', "Wait", "Creating advert...", 0);
            membersRepository.PostAdvert($scope.advert).then(function () {
                toaster.clear();
                toaster.pop('success', "Success", "Advert successfully created.");
                $scope.advert = {};
                $scope.locationEnabled = false;
                $scope.advertCreateForm.$setPristine(true);
            }).catch(function () {
                toaster.pop('error', "Error", "Something went wrong.");
            });
        };

    }]);