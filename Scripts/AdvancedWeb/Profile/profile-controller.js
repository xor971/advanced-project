﻿CoinsMarket.controller('profileController', ['$scope', '$routeParams', 'membersRepository', function ($scope, $routeParams, membersRepository) {
        //Profil Controller.
        
        $scope.$emit("changedPage", "Profile");
        
        $scope.userProfile = {};
        
        //Get the user data.
        membersRepository.GetProfile($routeParams.name).then(function (response) {
            $scope.userProfile = response;
        }).catch(function (error) {
            console.log("error = ", error);
        });
        
        $scope.edit = function () {
            $scope.editing = true;
        }
        
        $scope.save = function () {
            $scope.editing = false;
        }
    }]);