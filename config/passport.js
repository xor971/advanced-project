var mongoose		= require('mongoose');
var LocalStrategy   = require('passport-local').Strategy;
var User       		= mongoose.model('User');
var FacebookStrategy = require('passport-facebook').Strategy;

var FACEBOOK_APP_ID = "337083306497571"
var FACEBOOK_APP_SECRET = "0d68aafd4cf24a8c57df7fe26f043463";



module.exports = function(passport, my_express) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

//Connexion ou inscription Facebook.

    passport.use('facebook', new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "http://162.243.19.230/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    process.nextTick(function () {
      
      // To keep the example simple, the user's Facebook profile is returned to
      // represent the logged-in user.  In a typical application, you would want
      // to associate the Facebook account with a user record in your database,
      // and return that user instead.
      User.findOne({ 'email' :  profile.emails[0].value}, function(err, user) {
            if (err)
                return done(null, false);

            if (user)
                return done(null, user);

            User.findOne({ 'usename' :  profile.name.familyName}, function(err, user) {
                if (err)
                    return done(null, false);

                if (user)
                    return done(null, user);
        console.log(profile);
        var newUser       = new User();
        newUser.email     = profile.emails[0].value;
        newUser.password  = newUser.generateHash(profile.name.familyName);
        newUser.username  = profile.name.familyName;
        newUser.firstName = profile.name.givenName;
        newUser.name      = profile.name.familyName;
        newUser.active    = 0;
        newUser.dateSignup= new Date();
        newUser.lastLogged=null;
        newUser.save(function(err) {
                    if (err){
                            return res.json(400, err);
                    }
                });

      return done(null, newUser);
            }); 
        });

    });
  }
));

   
//Se logguer
    passport.use('my_login', new LocalStrategy({

        usernameField : 'email',
        passwordField : 'password',
    },
    function(email, password, done) { 
	
        User.findOne({ 'email' :  email}, function(err, user) { 
		
            if (err)
                return done(err);
            if (!user)
                return done(null, false);

            if (!user.validPassword(password))
                return done(null, false); 
			
            return done(null, user);
        });

    }));
	
	
};






