var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var http = require('http');
var engine = require('ejs-locals');
var mongoose = require('mongoose');
var configDB = require('./config/database.js')(mongoose);
var mongoStore = require('connect-mongodb');
var flash = require('connect-flash');
var passport = require('passport');
var path = require('path');
var fs = require('fs');
var my_express = express();



var modelsPath = path.join(__dirname, 'models');
fs.readdirSync(modelsPath).forEach(function (file) {
    require(modelsPath + '/' + file);
});

require('./config/passport')(passport, my_express);

my_express.engine('html', require('ejs').renderFile);
my_express.set('view engine', 'html');

my_express.set('port', process.env.PORT || 4242);
my_express.use(morgan('dev'));
my_express.use(cookieParser());
my_express.use(bodyParser());
my_express.use(methodOverride());

//


//Save session 
my_express.use(function (req, res, next) {
    expressSession({
        secret: 'pilowpiliwpala%}^mouchpalamichpasstipeu',
        cookie: { maxAge : (req.method == 'POST' && req.url == '/login') ? (req.body.rememberMe ? 604800000 /*<-uneSemaine*/: 86400000 /*<-24Heure*/) : 86400000 },
        store: new mongoStore({
            db: mongoose.connection.db,
            collection: 'sessions'
        })
    })(req, res, next);
}
);

//passport authentication

my_express.use('/Views',express.static('./Views'));
my_express.use('/Content', express.static('./Content'));
my_express.use('/Scripts', express.static('./Scripts'));
my_express.use(passport.initialize());
my_express.use(passport.session());
my_express.use(flash());

require('./routes/core.js')(my_express, passport);


//run the server
http.createServer(my_express).listen(my_express.get('port'), function () {
    console.log('Server listening on port ' + my_express.get('port'));
});

//my_express.listen(8080);
console.log("Server started\n");